package com.venusactivist;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.GetChars;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;


import com.facebook.AccessToken;
import com.facebook.AccessTokenSource;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.*;
import com.facebook.LoginActivity;
import com.facebook.widget.LoginButton;
import com.facebook.Response;

import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * 
 * @author Ben Matok
 * seems like need to understand life cycle better
 * save more stuff globally and check if they were initialized already
 * so on future init we won't leak.
 * 
 * this may explain double GUI stuff
 * notice re-init on rotate
 * 
 * 
 * TODO:
 * - ask publish permissions on init, and not on post.
 * - long time staying alive, move to service or somthing
 *   or give OS flag to restart app after killing it automatically
 *   also save app status correctly
 *   
 *
 */



public class MainActivity extends FragmentActivity {
	public static queryClickListener reciever = null;
	static boolean  askedWritePermissions = false;
	private MainFragment mainFragment;
	private static final String TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		  	Log.d(TAG,"onCreate");
			// IMPROTANT!! initialize any variable before call to super
			// or else data may be used by sub components (ie MainFragment )
		  	// NOTIVE next line allows to use resources so must call this first!
			setContentView(R.layout.activity_main);
			super.onCreate(savedInstanceState);
		if ( savedInstanceState == null ) {
		  	Log.d(TAG,"onCreate - NEW ");
			// Add the fragment on initial activity setup
	        mainFragment = new MainFragment();
	        getSupportFragmentManager()
	        .beginTransaction()
	        .add(android.R.id.content, mainFragment)
	        .commit(); 
		} else {
		  	Log.d(TAG,"onCreate - RESTORED ");
			// Or set the fragment from restored state info
	        mainFragment = (MainFragment) getSupportFragmentManager()
	        .findFragmentById(android.R.id.content);
			 
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  Log.d(TAG,"onActivityResult");
	  super.onActivityResult(requestCode, resultCode, data);
	  Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	
	@Override
    public void onBackPressed()
    {
        //super.onBackPressed();
		return;
    }
	
	public static class MainFragment extends Fragment {
		public MainFragment(){super();}
		private static final String TAG = "MainFragment";
		private UiLifecycleHelper uiHelper;
		private Session.StatusCallback callback = new Session.StatusCallback() {
		    @Override
		    public void call(Session session, SessionState state, Exception exception) {
		        onSessionStateChange(session, state, exception);
		    }
		};
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			Log.d(TAG,"onCreate");
		    super.onCreate(savedInstanceState);
		    uiHelper = new UiLifecycleHelper(getActivity(), callback);
		    uiHelper.onCreate(savedInstanceState);
		    Button queryButton = (Button)getActivity().findViewById(R.id.QueryButton);
		    if(queryButton==null){
				Log.d(TAG,"onCreate - queryButton == null!");
		    }
			Button multiQueryButton = (Button)getActivity().findViewById(R.id.MultiQueryButton);
			if(MainActivity.reciever !=null){
				Log.d(TAG,"reciever already initialized!");
				Log.d(TAG,reciever.toString());
			}
			queryClickListener autoShareListener = new queryClickListener(getActivity(),this,queryButton);
			MainActivity.reciever = autoShareListener;
			queryButton.setOnClickListener(autoShareListener);
			multiQueryButton.setOnClickListener(new multiQueryClickListener(this));
		}
		
		@Override
		public void onResume() {
			Log.d(TAG,"onResume");
		    super.onResume();
		    // For scenarios where the main activity is launched and user
		    // session is not null, the session state change notification
		    // may not be triggered. Trigger it if it's open/closed.
		    Session session = Session.getActiveSession();
		    if (session != null &&
		           (session.isOpened() || session.isClosed()) ) {
		        onSessionStateChange(session, session.getState(), null);
		    }

		    uiHelper.onResume();
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			Log.d(TAG,"onActivityResult");
		    super.onActivityResult(requestCode, resultCode, data);
		    uiHelper.onActivityResult(requestCode, resultCode, data);
		}

		@Override
		public void onPause() {
			Log.d(TAG,"onPause");
		    super.onPause();
		    uiHelper.onPause();
		}

		@Override
		public void onDestroy() {
			Log.d(TAG,"onDestroy");
		    uiHelper.onDestroy();
		    Button qButton = (Button) getActivity().findViewById(R.id.QueryButton);
		    //qButton.getContext().unregisterReceiver(MainActivity.reciever);
		    super.onDestroy();
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			Log.d(TAG,"onSaveInstanceState");
		    super.onSaveInstanceState(outState);
		    uiHelper.onSaveInstanceState(outState);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, 
		        ViewGroup container, 
		        Bundle savedInstanceState) {

			Log.d(TAG,"onCreateView");
		    View view = inflater.inflate(R.layout.activity_main, container, false);
		    LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
		    //authButton.
		    authButton.setFragment(this);
		    //authButton.setReadPermissions(Arrays.asList("user_likes", "user_status"));
		    return view;
		}
		
		private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
			Log.d(TAG,"onSessionStateChange");
		    if (state.isOpened()) {
		        Log.i(TAG, "Logged in...");
				//List<String> PERMISSIONS = Arrays.asList("user_likes", "user_status");
	            //session.requestNewReadPermissions(new Session.NewPermissionsRequest(this, PERMISSIONS));
		        
		        Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						Log.d("TVP","onCompleted");
						  if (user != null) {
							  Log.d("TVP","user!=null");
					    	  TextView welcome = (TextView) getActivity().findViewById(R.id.SimpleText);
							  Button qButton = (Button) getActivity().findViewById(R.id.QueryButton);
							  Button mqButton = (Button) getActivity().findViewById(R.id.MultiQueryButton);
							  ToggleButton tglButton = (ToggleButton) getActivity().findViewById(R.id.toggleButton1);
							  welcome.setVisibility(View.VISIBLE);
							  qButton.setVisibility(View.VISIBLE);
							  mqButton.setVisibility(View.VISIBLE);
						      tglButton.setVisibility(View.VISIBLE);
							  welcome.setText("Hello \n" + user.getName() + "!");
						  }
					}
		        });
		    } else if (state.isClosed()) {

		    	TextView welcome = (TextView) getActivity().findViewById(R.id.SimpleText);
				Button qButton = (Button) getActivity().findViewById(R.id.QueryButton);
				Button mqButton = (Button) getActivity().findViewById(R.id.MultiQueryButton);
				ToggleButton tglButton = (ToggleButton) getActivity().findViewById(R.id.toggleButton1);
		    	welcome.setVisibility(View.INVISIBLE);
		    	qButton.setVisibility(View.INVISIBLE);
		    	mqButton.setVisibility(View.INVISIBLE);
		    	tglButton.setVisibility(View.INVISIBLE);
		        Log.i(TAG, "Logged out...");
		    }
		}
	}
	
	public static void migrateFbTokenToSession(Context context) {
		Session facebook = Session.getActiveSession();
	    AccessToken accessToken = AccessToken.
	    		createFromExistingAccessToken(facebook.getAccessToken(),
	    				null, null, AccessTokenSource.FACEBOOK_APPLICATION_NATIVE, null);
	    Session.openActiveSessionWithAccessToken(context, accessToken, new Session.StatusCallback() {

	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	            Log.d("RENEW","fbshare Migration to newer state has been successful");
	            if(session != null && session.isOpened()) {
	                Session.setActiveSession(session);
	            }
	        }
	    });

	}
	
	public void checkKeys(){
		try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.venusactivist", 
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("Debug KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("Release KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT) );
            }
        } 
        catch (NameNotFoundException e) {} 
        catch (NoSuchAlgorithmException e) {}
	}
	
	public static class TimePickerFragment extends DialogFragment
    	implements TimePickerDialog.OnTimeSetListener {
	
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);
			
			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}
	
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user
			queryClickListener.customHour=hourOfDay;
			queryClickListener.customMinute=minute;
	    	queryClickListener.useCustomTime=true;
	    	// if enabled then set now, if not will set later when auto share clicked
	    	Button button = (Button) getActivity().findViewById(R.id.QueryButton);
	    	if(queryClickListener.enabled) {
	    		reciever.setCustomAlarm(button);
	    		if (!askedWritePermissions) {
		    		// ask permissions
	    			Log.d(TAG,"Ask Write");
			          MainActivity.askedWritePermissions = true;
			          Session session = 
			        		Session.getActiveSession()!=null?
			        				Session.getActiveSession():
			        				Session.openActiveSessionFromCache(getActivity().getBaseContext());
					  List<String> PERMISSIONS = Arrays.asList("publish_actions","publish_stream");
			          session.requestNewPublishPermissions(new Session.NewPermissionsRequest(getActivity(), PERMISSIONS));
	    		}
	    	}
	    	
			((ToggleButton) getActivity().findViewById(R.id.toggleButton1)).setChecked(true);
		}
	}
	public void onToggleClicked(View view) {
	    // always set unchecked
		// except when we actually recieve time ( see onTimeSet above )

		Button button = (Button)((Activity)view.getContext()).findViewById(R.id.QueryButton);
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	    	((ToggleButton) view).setChecked(false);
	    	queryClickListener.customHour=-1;
	    	queryClickListener.customMinute=-1;
	    	queryClickListener.useCustomTime=false;
	    	reciever.setCustomAlarm(button);
	        DialogFragment newFragment = new TimePickerFragment();
	        newFragment.show(getSupportFragmentManager(), "timePicker");
	    } else {
	    	queryClickListener.useCustomTime=false;
	    	reciever.cancelCustomAlarm(button);
	    }
	}

}

