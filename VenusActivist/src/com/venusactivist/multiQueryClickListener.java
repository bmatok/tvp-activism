package com.venusactivist;

import java.util.Arrays;
import java.util.List;

import javax.security.auth.Subject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;


/**
 * 
 * @author Ben Matok
 */

public class multiQueryClickListener implements OnClickListener {
	static final String TAG="TVP.MultiQuery";
	Fragment fragment;
	public multiQueryClickListener(Fragment fragment) {
		this.fragment=fragment;
	}
	@Override
    public void onClick(View v) {

		if(!MainActivity.askedWritePermissions) {
			Log.d(TAG,"Ask Write");
	          MainActivity.askedWritePermissions = true;
	          Session session = 
	        		Session.getActiveSession()!=null?
	        				Session.getActiveSession():
	        				Session.openActiveSessionFromCache(v.getContext());
			  List<String> PERMISSIONS = Arrays.asList("publish_actions","publish_stream");
	          session.requestNewPublishPermissions(new Session.NewPermissionsRequest((Activity)v.getContext(), PERMISSIONS));
		}
		
        String fqlQuery="{"+
        		"'VenusHead':'SELECT page_id FROM page WHERE username=\"DiscoverVenus\"',"+
        		"'VenusData':'SELECT message, attachment, permalink, created_time FROM stream WHERE source_id IN (SELECT page_id FROM #VenusHead) AND actor_id IN (SELECT page_id FROM #VenusHead) LIMIT 3',"+
        		"}";
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        Session session = Session.getActiveSession();
        Request request = new Request(session,
            "/fql",                         
            params,                         
            HttpMethod.GET,                 
            new Request.Callback(){
                public void onCompleted(Response response) {
                	
                	String permalink = getValue("permalink",response.toString()).replace("\\/", "/");
                	//String postId = getValue("post_id",response.toString()).replace("\\/", "/");
                	//String msg = getValue("message",response.toString()).replace("\\/", "/");
                	String msg = " ";
		            
		            Bundle params = new Bundle();
		            //params.putString("caption", "caption");
		            params.putString("message", msg);
		            params.putString("link", permalink);
		            Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST);
		            request.setCallback(new Request.Callback() {
		                @Override
		                public void onCompleted(Response response) {
		                    if (response.getError() == null) {
		                        Log.i(TAG, "POST SUCCESS!");
		                        Toast.makeText(fragment.getActivity(), "Successful post!", Toast.LENGTH_LONG).show();
		                    } else {
		                        Toast.makeText(fragment.getActivity(), "ERROR!", Toast.LENGTH_LONG).show();
		                        Log.e(TAG, response.getError().toString());
		                    }
		                }
		            });
		            request.executeAsync();
                	/**
		            Request likeRequest = new Request(Session.getActiveSession(), postId + "/likes", null, HttpMethod.POST, new Request.Callback() {
	
			                @Override
			                public void onCompleted(Response response) {
			                    if (response.getError() == null) {
			                        Log.i(TAG, "LIKE SUCCESS!");
			                        Toast.makeText(fragment.getActivity(), "Successful Like!", Toast.LENGTH_LONG).show();
			                    } else {
			                        Toast.makeText(fragment.getActivity(), "ERROR!", Toast.LENGTH_LONG).show();
			                        Log.e(TAG, response.getError().toString());
			                    }
			                }
			        });
		            likeRequest.executeAsync();
		            **/
                }
        });
        Log.i("FQL",request.toString());
        Request.executeBatchAsync(request);
    }

	public String getValue(String field,String raw){
		Log.i("RAW RESPONSE",raw);
    	String str = raw.toString();
    	String name="\""+field+"\":";
    	String value="";
    	
    	// search as long as we find empty value
    	try{
	    	while(value.equals("")) {
		    	int idx = str.indexOf(name);
		    	if(idx==-1) break;
		    	str = str.substring(idx+name.length());
		    	
		    	idx = Math.min(str.indexOf(","),str.indexOf("}"));
		    	if(idx==-1) break;
		    	if(idx>2) // only case of non empty string
		    		value = str.substring(1,idx-1);
		    	str = str.substring(idx);
	    	}
    	} catch(NullPointerException e){
    		return "";
    	}
    	return value;
	}
	
}
