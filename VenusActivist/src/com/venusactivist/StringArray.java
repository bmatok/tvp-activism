package com.venusactivist;

/**
 * 
 * @author Ben Matok
 * 
 */

import android.content.Context;

public class StringArray {
    private static String theArray;
    public static String get(Context context) {
        if (theArray == null) {
            theArray = context.getResources().getString(R.string.app_id);
        }
        return theArray;
    }
}