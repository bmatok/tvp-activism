package com.venusactivist;


import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * 
 * @author Ben Matok
 */

public class queryClickListener extends BroadcastReceiver implements OnClickListener {

	static final String GenericMessages[] = {
		"�� ����� ���� �������! �� �� �� ��� ���� �� ��� ��� ����� ���� ��������",
		"��� �������!",
		"���� ����� ����� �����!",
		"�����",
		"�����",
		"����� ���� �����",
		"��� ����� ����� ����� ��������!",
		"����� �� ����",
		"���� ����, ����� �����. ��� ����� �� ���� ����",
		"������",
		"�� ��� ����",
		"���� ������� ���� ���",
		"��� �� ����� �� ��",
		"��� ��� ���� ����� ���",
		"����� ������ ����"
		};
	
	/**
	 * 
	 * 
Request likeRequest = new Request(Session.getActiveSession(), fBPostId + "/likes", null, HttpMethod.POST, new Request.Callback() {

         @Override
         public void onCompleted(Response response) {
                Log.i(TAG, response.toString());
         }
});
Request.executeBatchAndWait(likeRequest);
	 */
	
	public static final String envVarEnabledAutoShare="TVPactivismAutoShare";
	
	static final String TAG="TVP.Query";
	public static boolean enabled=false;
	public static boolean useCustomTime=false; 
	
	public static int customHour=0;
	public static int customMinute=0;
	Random rand;
	
	public queryClickListener(){
		rand = new Random(Calendar.getInstance().getTimeInMillis());
        Log.d(TAG, "Called Empty Constructur");
	}
	
	public queryClickListener(Activity activity,Fragment fragment,Button button) {
		rand = new Random(Calendar.getInstance().getTimeInMillis());

		String sEnabled = System.getProperty(envVarEnabledAutoShare);
		if(sEnabled != null && sEnabled.equals("true")){
			button.setText("Auto Share is ENABLED");
			button.setBackgroundColor(Color.GREEN);
		} else {
			button.setText("Auto Share is DISABLED");
			button.setBackgroundColor(Color.RED);
		}
		//button.getContext().registerReceiver(this, new IntentFilter());
	}
	
	@Override
	public void onClick(View v) {
		enabled = !enabled;
		Button button = (Button)((Activity)v.getContext()).findViewById(R.id.QueryButton);
		if(enabled){
			System.setProperty(envVarEnabledAutoShare, "true");
			button.setText("Auto Share is ENABLED");
			button.setBackgroundColor(Color.GREEN);
			if(!MainActivity.askedWritePermissions) {
    			Log.d(TAG,"Ask Write");
		          MainActivity.askedWritePermissions = true;
		          Session session = 
		        		Session.getActiveSession()!=null?
		        				Session.getActiveSession():
		        				Session.openActiveSessionFromCache(v.getContext());
				  //List<String> PERMISSIONS = Arrays.asList("publish_actions","publish_stream");
		          //session.requestNewPublishPermissions(new Session.NewPermissionsRequest((Activity)v.getContext(), PERMISSIONS));
			}
	          
			setAlarm(button);
            Log.i(TAG, "ALARM SET");
		} else {
			System.setProperty(envVarEnabledAutoShare, "false");
			button.setText("Auto Share is DISABLED");
			button.setBackgroundColor(Color.RED);
			cancelAlarm(button);
            Log.i(TAG, "ALARM CANCELED");
		}
		
		
    }
	
	
		public void shareLatest(final Context context){
			int idx = rand.nextInt(GenericMessages.length);
			shareLatest(context,GenericMessages[idx]);
			
		}
		public void shareLatest(final Context context,final String str){
			 String fqlQuery="{"+
		        		"'VenusHead':'SELECT page_id FROM page WHERE username=\"DiscoverVenus\"',"+
		        		"'VenusData':'SELECT message, attachment, permalink, created_time FROM stream WHERE source_id IN (SELECT page_id FROM #VenusHead) AND actor_id IN (SELECT page_id FROM #VenusHead) LIMIT 3',"+
		        		"}";
		        Bundle params = new Bundle();
		        params.putString("q", fqlQuery);
		        //Session session = Session.getActiveSession();
		        Session session = 
		        		Session.getActiveSession()!=null?
		        				Session.getActiveSession():
		        				Session.openActiveSessionFromCache(context);
		        Request request = new Request(session,
		            "/fql",
		            params,
		            HttpMethod.GET,
		            new Request.Callback(){
		                public void onCompleted(Response response) {
		                	
		                	String permalink = getValue("permalink",response.toString()).replace("\\/", "/");
		                	//String msg = getValue("message",response.toString()).replace("\\/", "/");
		                	String msg = str;
		                	
							//Facebook authenticatedFacebook = new Facebook(getString(R.string.app_id));
		    		        Session session = 
		    		        		Session.getActiveSession()!=null?
		    		        				Session.getActiveSession():
		    		        				Session.openActiveSessionFromCache(context);
							//List<String> PERMISSIONS = Arrays.asList("publish_actions","publish_stream");
				            //session.requestNewPublishPermissions(new Session.NewPermissionsRequest((Activity)context, PERMISSIONS));
				            
				            
				            
				            Bundle params = new Bundle();
				            //params.putString("caption", "caption");
				            params.putString("message", msg);
				            params.putString("link", permalink);
	
				            Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST);
				            request.setCallback(new Request.Callback() {
				                @Override
				                public void onCompleted(Response response) {
				                    if (response.getError() == null) {
				                        Log.i(TAG, "POST SUCCESS!");
				                        // Display message on screen
				                        Toast.makeText(context, "Successful post!", Toast.LENGTH_LONG).show();  
				                    } else {
				                    	Log.e(TAG,response.getError().toString());
				                        Toast.makeText(context, "ERROR!", Toast.LENGTH_LONG).show();
				                    }
				                }
				            });
				            request.executeAsync();
		                }                  
		        }); 
		        Request.executeBatchAsync(request);
		}
		
		public String getValue(String field,String raw){
			Log.i("RAW RESPONSE",raw);
	    	String str = raw.toString();
	    	String name="\""+field+"\":";
	    	String value="";
	    	
	    	// search as long as we find empty value
	    	try{
		    	while(value.equals("")) {
			    	int idx = str.indexOf(name);
			    	if(idx==-1) break;
			    	str = str.substring(idx+name.length());
			    	
			    	idx = Math.min(str.indexOf(","),str.indexOf("}"));
			    	if(idx==-1) break;
			    	if(idx>2) // only case of non empty string
			    		value = str.substring(1,idx-1);
			    	str = str.substring(idx);
		    	}
	    	} catch(NullPointerException e){
	    		return "";
	    	}
	    	return value;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO add more correct result for enabled disabled
			String sEnabled = System.getProperty(envVarEnabledAutoShare);
			if(sEnabled != null && sEnabled.equals("true")) {
				Log.d(TAG, "in recieve and ENABLED");
			} else {
				Log.d(TAG, "in recieve but DISABLED");
			}

            if(intent==null){
            	Log.d(TAG, "intent NULL");
            	return;
            }

            
			if(intent.getAction() != null && intent.getAction().equals(s1102AM)){
				shareLatest(context);
                Log.i("ALARM RECIEVE", "1102AM");
			} else if(intent.getAction() != null &&  intent.getAction().equals(s0332PM)){
				shareLatest(context);
                Log.i("ALARM RECIEVE", "332PM");
			} else if(intent.getAction() != null &&  intent.getAction().equals(sCUSTOMTIME)){
				shareLatest(context);
                Log.i("ALARM RECIEVE", "CustomTime");
			} else {
                Log.i("ALARM RECIEVE", "Not our intent");
			}
		}

	
	public static final int _1102AM=1102269;
	public static final int _0332PM= 332269;
	public static final int _CUSTOMTIME= 123332211;
	public static final String s1102AM="1102AM";
	public static final String s0332PM="0332PM";
	public static final String sCUSTOMTIME="TVPCUSTOMTIME";
	

	public void cancelAlarm(Button button){
		
		AlarmManager am = (AlarmManager) button.getContext().getSystemService(Context.ALARM_SERVICE);

		Intent i1102AM = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);
		Intent i0332PM = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);
		Intent iCustom = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);

		i1102AM.setAction(s1102AM);
		i0332PM.setAction(s0332PM);
		iCustom.setAction(sCUSTOMTIME);
		
		PendingIntent pi1102AM = PendingIntent.getBroadcast(button.getContext(), _1102AM	, i1102AM, PendingIntent.FLAG_CANCEL_CURRENT);
		PendingIntent pi0332PM = PendingIntent.getBroadcast(button.getContext(), _0332PM	, i0332PM, PendingIntent.FLAG_CANCEL_CURRENT);
		PendingIntent piCustom = PendingIntent.getBroadcast(button.getContext(), _CUSTOMTIME, iCustom, PendingIntent.FLAG_CANCEL_CURRENT);
		
		am.cancel(pi1102AM); // cancel any existing alarms
		pi1102AM.cancel();
		am.cancel(pi0332PM); // cancel any existing alarms
		pi0332PM.cancel();
		am.cancel(piCustom); // cancel any existing alarms
		piCustom.cancel();
	}
	public void setAlarm(Button button){
		AlarmManager am = (AlarmManager) button.getContext().getSystemService(Context.ALARM_SERVICE);
		
		Intent i1102AM = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);
		Intent i0332PM = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);
		Intent iCustom = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);

		i1102AM.setAction(s1102AM);
		i0332PM.setAction(s0332PM);
		iCustom.setAction(sCUSTOMTIME);
		
		PendingIntent pi1102AM = PendingIntent.getBroadcast(button.getContext(), _1102AM, i1102AM,  PendingIntent.FLAG_CANCEL_CURRENT );
		PendingIntent pi0332PM = PendingIntent.getBroadcast(button.getContext(), _0332PM, i0332PM, PendingIntent. FLAG_CANCEL_CURRENT );
		PendingIntent piCustom = PendingIntent.getBroadcast(button.getContext(), _CUSTOMTIME, iCustom, PendingIntent.FLAG_CANCEL_CURRENT);
		
		am.cancel(pi1102AM); // cancel any existing alarms
		am.cancel(pi0332PM); // cancel any existing alarms
		am.cancel(piCustom); // cancel any existing alarms
		
		int rSeconds = rand.nextInt(60);
		int rMinutes = rand.nextInt(3);
		
		Calendar cal1102AM=Calendar.getInstance();
		cal1102AM.set(Calendar.HOUR_OF_DAY,11);
		cal1102AM.set(Calendar.MINUTE,02);
		cal1102AM.add(Calendar.SECOND,rSeconds);
		cal1102AM.add(Calendar.MINUTE,rMinutes);
		if(Calendar.getInstance().after(cal1102AM)){
            Log.d("SetAlarm", "After - 1102AM");
			cal1102AM.add(Calendar.DATE, 1);
		}

		Calendar cal0332PM=Calendar.getInstance();
		cal0332PM.set(Calendar.HOUR_OF_DAY,15);
		cal0332PM.set(Calendar.MINUTE,32);
		cal0332PM.add(Calendar.SECOND,rSeconds);
		cal0332PM.add(Calendar.MINUTE,rMinutes);
		if(Calendar.getInstance().after(cal0332PM)){
            Log.d("SetAlarm", "After - 0332PM");
			cal0332PM.add(Calendar.DATE, 1);
		}
		

		Calendar calCustom=Calendar.getInstance();
		calCustom.set(Calendar.HOUR_OF_DAY,customHour);
		calCustom.set(Calendar.MINUTE,customMinute);
		if(Calendar.getInstance().after(calCustom)){
            Log.d("SetAlarm", "After - CUSTOM");
            calCustom.add(Calendar.DATE, 1);
		}

		long day=1000L*60L*60L*24L;
		am.setRepeating(AlarmManager.RTC_WAKEUP, cal1102AM.getTimeInMillis(), day, pi1102AM);
		am.setRepeating(AlarmManager.RTC_WAKEUP, cal0332PM.getTimeInMillis(), day, pi0332PM);
		Log.d("SetAlarm", "1102AM -"+cal1102AM);
		Log.d("SetAlarm", "0332PM -"+cal0332PM);
		
		if(queryClickListener.useCustomTime){
			am.setRepeating(AlarmManager.RTC_WAKEUP, calCustom.getTimeInMillis(), day, piCustom);
			Log.d("SetAlarm", "CUSTOM -"+calCustom);
		}
	}
	
	public void setCustomAlarm(Button button){
		AlarmManager am = (AlarmManager) button.getContext().getSystemService(Context.ALARM_SERVICE);
		Intent iCustom = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);
		iCustom.setAction(sCUSTOMTIME);
		PendingIntent piCustom = PendingIntent.getBroadcast(button.getContext(), _CUSTOMTIME, iCustom, PendingIntent.FLAG_CANCEL_CURRENT);
		am.cancel(piCustom); // cancel any existing alarms
		
		Calendar calCustom=Calendar.getInstance();
		calCustom.set(Calendar.HOUR_OF_DAY,customHour);
		calCustom.set(Calendar.MINUTE,customMinute);
		if(Calendar.getInstance().after(calCustom)){
            Log.d("SetAlarm", "After - CUSTOM");
            calCustom.add(Calendar.DATE, 1);
		}
		long day=1000L*60L*60L*24L;
		am.setRepeating(AlarmManager.RTC_WAKEUP, calCustom.getTimeInMillis(), day, piCustom);
		Log.d("SetAlarm", "CUSTOM -"+calCustom);
	}
	

	public void cancelCustomAlarm(Button button){
		AlarmManager am = (AlarmManager) button.getContext().getSystemService(Context.ALARM_SERVICE);
		Intent iCustom = new Intent(button.getContext(),com.venusactivist.queryClickListener.class);

		iCustom.setAction(sCUSTOMTIME);
		
		PendingIntent piCustom = PendingIntent.getBroadcast(button.getContext(), _CUSTOMTIME, iCustom, PendingIntent.FLAG_CANCEL_CURRENT);
		
		am.cancel(piCustom); // cancel any existing alarms
		piCustom.cancel();
	}
	
	

}
